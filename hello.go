package main

import (
	"fmt"
	"math"
	"math/rand"
)

const (
	// Big Create a huge number by shifting a 1 bit left 100 places.
	// In other words, the binary number that is 1 followed by 100 zeroes.
	Big = 1 << 100
	// Small Shift it right again 99 places, so we end up with 1<<1, or 2.
	Small = Big >> 99
)

func main() {

	//basic
	fmt.Println("Begin basic part")
	a := 3
	b := &a
	log(*b)

	// random
	fmt.Println("")
	fmt.Println("Begin math part")
	rand.Seed(2)
	fmt.Printf("random -> %d\n", rand.Intn(10))
	fmt.Printf("square root -> %f\n", math.Sqrt(10))
	fmt.Printf("Pi -> %f\n", math.Pi)

	//function
	fmt.Println("")
	fmt.Println("Begin funtion part")
	c, d := 3, 4
	fmt.Printf("function add integer of %d and %d -> %d\n", c, d, add(c, d))

	//type casting
	fmt.Println("")
	fmt.Println("Begin type casting part")
	e := 7
	f := float64(e)
	fmt.Printf("casting integer %d into float %f\n", e, f)

	//constant
	fmt.Println("")
	fmt.Println("Begin constant precision part")
	g := Small
	var h, l float64 = Small, Big

	fmt.Printf("using contant integer -> %d and as float -> %f %f\n", g, h, l)

	for i := 1; i <= 10; i++ {
		rand.Seed(int64(i))
		m := rand.Intn(i)
		fmt.Printf("random -> %d\n", m)
		log(m)
	}
}

func log(a int) {
	fmt.Printf("address %X and value %d \n", &a, a)
}

func add(a, b int) int {
	var result = a + b
	log(result)
	return result
}
